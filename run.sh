#!/bin/sh

# If no arguments are supplies, open the shell
if [ $# -eq 0 ]
    then
        docker-compose -f docker-compose.yml run -w /var/tarantool/app app-shell /bin/bash
    else
        docker-compose -f docker-compose.yml run -w /var/tarantool/app app-shell /usr/bin/env
"$@"
fi