---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by chris.
--- DateTime: 01/04/2021 7:09 PM
---

require 'busted.runner'()
local json = require('json')
local error_constants = require('constants.errors')

local student_list = {
    {
        lastname= "Vita",
        student_num= "7657063",
        year_level= 4,
        email_address= "cjqvita@gmail.com",
        firstname= "Chris",
        phone_num= "09612354348",
        birthdate= "1994-03-23"
    }
}

local successful_response = {
    status = 200,
    headers = { ['content-type'] = 'application/vnd.api+json' },
    body = json.encode(student_list)
}

local invalid_year_level_response = {
    status = 400,
    headers = { ['content-type'] = 'application/vnd.api+json' },
    body = json.encode({error_msg = error_constants.INVALID_YEAR_LEVEL})
}

local db_error_response = {
    status = 400,
    headers = { ['content-type'] = 'application/vnd.api+json' },
    body = json.encode({error_msg = error_constants.INTERNAL_ERROR})
}

describe("Get Student List tests #unit", function()
    local test_cases = {
        ['Invalid year level'] = {
            request = {year_level = '4'},
            expected_response = invalid_year_level_response
        },
        ['DB error'] = {
            request = {year_level = 4},
            mock_get = function() error('DB error') end,
            expected_response = db_error_response
        },
        ['Successful call with empty request'] = {
            request = {},
            mock_get = function() return student_list end,
            expected_response = successful_response
        },
        ['Successful call with valid request'] = {
            request = {year_level = 4},
            mock_get = function() return student_list end,
            expected_response = successful_response
        }
    }

    for test_case_name, case in pairs(test_cases) do
        insulate("-", function()
            it(test_case_name, function()
                local mock_student_entity = require('models.student')
                function case.request.json()
                    return case.request
                end
                mock_student_entity.get = case.mock_get
                package.loaded['models.student'] = mock_student_entity

                local get_student_list_handler = require('controllers.get-student-list-handler')
                local response = get_student_list_handler.get_list(case.request)

                assert.are.same(case.expected_response, response)
            end)
        end)
    end
end)