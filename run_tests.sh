#!/usr/bin/env bash

if [[ -n "$1" ]]
then
#  find tests -name '*.lua' -exec tarantool {} $1;
    find tests -type f -name '*.lua' -print0 | while IFS= read -r -d '' file; do
        tarantool "$file" "--tags=$1"
        rm -r -f tnt_test
    done
else
  echo "Running all tests..."
  find tests -name '*.lua' | xargs tarantool
fi
