
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE students (
    student_num varchar(7) primary key,
    firstname varchar(50) not null,
    lastname varchar(50) not null,
    year_level smallint not null,
    email_address varchar(255),
    phone_num varchar(50),
    birthdate date not null
);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE students;
